// caches are send as [][] (for each id string defined as [x][0] send also title [x][1] and description [2])
// this approach allows to send additional data in the future
var uris = [];

function ajaxFilePost(tempDirName) {
    var formData = new FormData(document.forms.namedItem("fileForm"));
    $.ajax({
        url: '/photo/cache/add',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function(msg) {
            uris[uris.length] = msg;
            var html =
                "<div hidden id='" + msg + "' class='row'><div class='col-md-3'><img id='image" + msg + "' src='/resources/photos/" +  tempDirName + "/" + msg + "' class='photo-cache'/>" +
                    "<a style='cursor: pointer' onclick='deleteCache(\"" + msg + "\")'><span class='delete-cache'>x</span></a></div>" +
                    "<div class='col-md-5'>" +
                    "<div class='form-group'>" +
                    "<label for='title" + msg + "' class='control-label'>Nazwa</label>" +
                    "<input id='name" + msg + "' type='text' class='form-control'/>" +
                    "</div>" +
                    "<div class='form-group'>" +
                    "<label for='description" + msg + "' class='control-label'>Opis</label>" +
                    "<textarea class='description form-control' id='description" + msg + "' cols='40' rows='4'></textarea>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
            $('#caches').after(html);
            $('#saveButton').removeClass("disabled");
            $('#' + msg).animate({"opacity": "show", "height": "auto"}, 500);
        }).always(function(msg) {
        }).fail(function(msg) {
            alert("Error uploading: " + msg);
        });
}

function deleteCache(id) {
    $.post('/photo/cache/delete', {cache:id}, function(data, status) {
        var index = $.inArray(id, uris);
        if (index>=0) uris.splice(index, 1);
        var cache = $('#' + id);
        cache.animate({"opacity": "hide", "height": "0"}, 500, function() {cache.empty()});
    });
}
function saveCaches() {
    // TODO: for each cache create map {uri:'', name:'', description:''}
    if(uris.length == 0) {
        alert("no uris to save");
        return;
    }
    var caches = [];
    uris.forEach(function(entry) {
        var jsonCache = {
            "uri": entry,
            "name": document.getElementById('name' + entry).value,
            "description": document.getElementById('description' + entry).value
        };
        caches.push(jsonCache);
    });

    $.ajax({
        type: 'POST',
        url: '/photo/save',
        data: JSON.stringify(caches),
        contentType: "application/json",
    success : function(data) {
        if(data == 'ok') {
            window.location.replace("/dashboard");
        } else {
            alert("Error");
        }
    },
    error : function(data) {
            alert("error: " + data);
    }
    });
}

$(document).ready(function() {
    $.ajaxSetup({
        'beforeSend': function() {
            $('#loading').show();
            $('#saveButton').addClass("disabled");
        },
        'complete': function() {
            $('#loading').hide();
            $('#saveButton').removeClass("disabled");
        }
    });
});
