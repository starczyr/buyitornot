<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="user" required="true" type="com.herokuapp.buyItOrNot.user.model.User" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
</head>
<body>
<div id="menu">

    <c:set value="${sessionScope.loggedInUser}" var="loggedInUser"/>

    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><fmt:message key="app.name"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/${loggedInUser.accountName}">
                      <c:choose>
                        <c:when test="${loggedInUser.name eq '' or loggedInUser.lastName eq ''}">
                            ${loggedInUser.accountName}
                        </c:when>
                        <c:otherwise>
                          ${loggedInUser.name} ${loggedInUser.lastName}
                        </c:otherwise>
                      </c:choose>
                    </a></li>
            </ul>

            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                    <button type="button" class="btn btn-default navbar-btn dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-picture glyphicon"></span><b class="caret"></b>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="/photo/upload">
                            <span class="glyphicon glyphicon-plus"></span> <fmt:message key="menu.option.addphoto"/></a>
                        </li>
                        <li><a href="" class="disabled">All photos</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                <button type="button" class="btn btn-default navbar-btn dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user"></span><b class="caret"></b>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/allusers"><fmt:message key="dashboard.menu.allusers"/></a>
                    </li>
                    <li><a href="" class="disabled">Observed</a></li>
                    <li><a href="/${loggedInUser.accountName}">Me</a></li>
                </ul>
            </li>
            </ul>

            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>

                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-cog"></span><b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/app/credits"><fmt:message key="menu.option.credits"/></a></li>
                        <li class="divider"></li>
                        <li><a href="/static/j_spring_security_logout">Log out</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-flag"></span><b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/app/locale/en"><fmt:message key="locale.english"/></a></li>
                        <li class="divider"></li>
                        <li><a href="/app/locale/pl"><fmt:message key="locale.polski"/></a></li>
                    </ul>
                </li>
            </ul>


        </div><!-- /.navbar-collapse -->
    </nav>
</div>
</body>
</html>