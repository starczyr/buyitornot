<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ attribute name="comment" required="true" type="com.herokuapp.buyItOrNot.comment.Comment" %>
<html>
<head>
</head>
<body>
    <div class="row">
        <div class="col-md-2">
            <c:choose>
                <c:when test="${comment.author.profilePhotoUrl ne null}">
                    <a href="/${comment.author.accountName}">
                        <img src="${comment.author.profilePhotoUrl}" class="miniature"/>
                    </a>
                </c:when>
                <c:otherwise>
                <a href="/${comment.author.accountName}">
                    <img src="/resources/images/default_user_icon.jpg" class="miniature"/>
                </a>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-10">
            <div class="row">
            <a href="/${comment.author.accountName}">
            <b>
                <c:choose>
                    <c:when test="${comment.author.name ne '' and comment.author.name ne null
                    and comment.author.lastName ne '' and comment.author.lastName ne null}">
                        <c:out value="${comment.author.name} ${comment.author.lastName}"/>
                    </c:when>
                    <c:otherwise>
                        <c:out value="${comment.author.accountName}"/>
                    </c:otherwise>
                </c:choose>

                </b>
                </a>
            </div>
            <div class="row">
              ${comment.content}
            </div>
            <span class="time">
                TODO: work on time counter
                <%--<c:out value="${timeCounter.countTimeSince(comment.addedOn)}" />--%>
            </span>
            <a><span class="icon-sm glyphicon glyphicon-ok"></span></a>
            <a><span class="icon-sm glyphicon glyphicon-remove"></span></a>

        </div>
    </div>
</body>
</html>





