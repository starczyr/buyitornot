<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="File upload modal" pageEncoding="UTF-8"%>

<html>
<head>
</head>
<body>
<div id="fileModal">
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">
                    <h3>Please, note that this is deprecated since it's better to upload from different URL</h3>
                    <!-- Modal -->
                    <fmt:message key="filemodal.title.upload"/></h4>
            </div>

            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li><a href="#file" data-toggle="tab"><fmt:message key="filemodal.tab.file"/></a></li>
                    <li><a href="#url" data-toggle="tab"><fmt:message key="filemodal.tab.url"/></a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="file">
                        <form id="form" name="form" method="post" enctype="multipart/form-data">
                            <div class="btn btn-primary">
                                <input id="fileInput" type="file" name="file" onchange="ajaxFilePost('${tempDirName}')"/>

                            </div>

                        </form>
                        <div id="caches">

                        </div>
                        
                    </div>
                    <div class="tab-pane" id="url">
                          <%--<label for="urlInput"><fmt:message key="filemodal.tab.url"/></label>--%>
                          <input type="text">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <img id="loading" class="ajax-loader pull-left" src="/resources/images/477.GIF"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="filemodal.button.close"/></button>
                <button id="saveButton" type="button"  class="btn btn-primary disabled"
                        onclick="saveCaches()"><fmt:message key="filemodal.button.savechanges"/></button>
            </div>

        </div>
    </div>
  </div>
</div>
<script src="http://code.jquery.com/jquery.js"></script>
<script>
    var $loading = $('#loading').hide();
    var i = 0;
    var caches = [];

    function ajaxFilePost(tempDirName) {
        var formData = new FormData(document.forms.namedItem("form"));

        $.ajax({
            url: '/photo/cache/add',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(msg) {
           caches[i++] = msg;
           var html = "<img src='/resources/photos/" +  tempDirName + "/" + msg + "' class='miniature'/>";
           $('#caches').after(html);
           $('#saveButton').removeClass("disabled");
        }).always(function(msg) {
        }).fail(function(msg) {
           alert("Error uploading: " + msg);
        });
    }

    function saveCaches() {
        if(caches.length == 0) {
            alert("no caches to save");
            return;
        }
        var formData = new FormData();
        formData.append("uris", caches);

        $.ajax({
            url: '/photo/save',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function() {
            alert("success");
        }).fail(function() {
             alert("Failure");
        });
    }
    $(document).ready(function() {
        $.ajaxSetup({
            'beforeSend': function() {
                $loading.show();
            },
            'complete': function() {
                $loading.hide();
            }
        });
    });

</script>
</body>
</html>