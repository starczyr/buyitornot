<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ attribute name="photo" required="true" type="com.herokuapp.buyItOrNot.photo.Photo" %>
<%@ attribute name="displayPhotoAuthor" required="false" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<body>
<security:authentication property="principal.username" var="mail"/>
<div class="row">
    <div class="col-md-6">
        <br/>

        <div class="row text-center">
            <c:if test="${displayPhotoAuthor eq true}">
                <div class="row text-left">
                    <a href="/${photo.owner.accountName}" class="no-underline">
                        <tags:miniature user="${photo.owner}"/>
                    </a>
                </div>
            </c:if>

            <c:if test="${photo.owner.mail ne mail}">
                <a onclick="voteForYes(${photo.id})" class="icon-lg no-underline">
                    <span class="icon-lg glyphicon glyphicon-plus"></span>
                </a>
                <a onclick="voteForNo(${photo.id})" class="icon-lg no-underline">
                    <span class="icon-lg glyphicon glyphicon-minus"></span>
                </a>
            </c:if>
            <c:if test="${photo.owner.mail eq mail}">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle no-underline" data-toggle="dropdown">
                        <span class="icon-lg glyphicon glyphicon-wrench"></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/photo/${photo.id}/bought">Mark as Bought</a></li>
                        <li><a href="/photo/${photo.id}/didntBuy">Mark as didn't buy</a></li>
                        <li class="divider"></li>
                        <li><a href="">Delete</a></li>
                    </ul>
                </div>
            </c:if>
            <div class="well well-sm">
                <a href="/photo/${photo.id}">
                    <img src="${photo.url}" class="img-responsive photo"/>
                </a>
            </div>
        </div>

        <div class="row">
            <a class="no-underline" onclick="showCommentTextArea('${photo.id}')" style="cursor: pointer;">
                <span class="icon-lg glyphicon glyphicon-comment"></span>
            </a>

            <a class="center no-underline pull-right" onclick="showComments('${photo.id}')" style="cursor: pointer;">Comments</a>

        </div>


        <div id="comments${photo.id}" class="row" hidden>
            <c:forEach items="${photo.comments}" var="comment">
                <tags:comment comment="${comment}"/>
            </c:forEach>
        </div>

        <div class="row">
            <form method="post" id="commentForm${photo.id}" hidden action="/photo/${photo.id}/comment/add">
                <div class="form-group">

                    <textarea cols="40" rows="3" name="comment" placeholder="<fmt:message key='photo.placeholder.yourcomment'/>"
                              class="form-control comment-text-area"></textarea>
                    <a onclick="hideCommentTextArea('${photo.id}')">
                        <span class="glyphicon glyphicon-remove pull-right"></span>
                    </a>
                    <button type="submit" class="btn btn-primary"><fmt:message key="photo.button.post"/></button>
                    <img id="loading" src="/resources/images/477.GIF" class="ajax-loader" hidden/>
                </div>
            </form>
        </div>
        <br/>
    </div>
</div>
<script>
    var $loading = $('#loading');

    function showCommentTextArea(id) {
        var id = "#commentForm" + id;

        $(id).show();
        var verticalCener = Math.floor(window.innerHeight / 2);
        $('html, body')
                .animate({scrollTop: $(id).offset().top - verticalCener}, 500);
    }

    function showComments(id) {
        var id = "#comments" + id;

        $(id).show();
        $('html, body')
                .animate({scrollTop: $(id).offset().top}, 500);
    }
    function hideCommentTextArea(id) {
        var id = "#commentForm" + id;
        $(id).hide();
    }

    function voteForYes(id) {

        vote('YES', id);
    }
    function voteForNo(id) {
        vote('NO', id);
    }

    function vote(voteFor, id) {
        var formData = new FormData();
        formData.append("vote", voteFor);

        var url = '/photo/' + id + '/vote';
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function () {
                    alert('success');
                }).fail(function () {
                    alert("Error");
                });
    }

    $(document).ready(function () {
        $.ajaxSetup({
            'beforeSend': function () {
                $loading.show();
            },
            'complete': function () {
                $loading.hide();
            }
        })
    });

</script>
</body>
</html>