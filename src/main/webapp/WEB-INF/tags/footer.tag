<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<html>
<head>
</head>
<body>
<div id="footer">
    <div class="row">
        <span>
          <fmt:message key="index.created">
            <fmt:param value="Radek Starczynowski"/>
          </fmt:message>
        </span>
    </div>
    <div class="row">
        <jsp:doBody/>
    </div>
</div>
</body>
</html>