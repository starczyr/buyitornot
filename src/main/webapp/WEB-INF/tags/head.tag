<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="title" required="false"%>
<html>
<head>
    <title>${title}<c:if test="${title != null}"> - </c:if><fmt:message key="app.name"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/resources/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/resources/css/buyornot.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="shortcut icon" href="/resources/icons/accept-or-decline-favicon.ico">

    <jsp:doBody/>
</head>
<body>

</body>
</html>