<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ attribute name="user" type="com.herokuapp.buyItOrNot.user.model.User" required="true" %>
<%@ attribute name="displayUserName" required="false" %>
<html>
<head>
</head>
<body>

<c:if test="${displayUserName eq true}">
    <c:choose>
        <c:when test="${user.name ne null and user.name ne '' and user.lastName ne null and user.lastName ne ''}">
            <c:set var="text" value="${user.name} ${user.lastName}"/>
        </c:when>
        <c:otherwise>
            <c:set var="text" value="${user.accountName}"/>
        </c:otherwise>
    </c:choose>
</c:if>
<div class="row">
  <a id="image" href="/${user.accountName}" class="no-underline">
      <c:choose>
          <c:when test="${user.profilePhotoUrl ne null}">
              <img src="${user.profilePhotoUrl}" class="photo-thumbnail img-responsive"/>
          </c:when>
          <c:otherwise>
              <img src="/resources/images/default_user_icon.jpg" class="photo-thumbnail"/>
          </c:otherwise>
      </c:choose>
  </a>
    <c:out value="${text}"/>
</div>
</body>
</html>