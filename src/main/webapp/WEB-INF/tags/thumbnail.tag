<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ attribute name="photo" required="true" type="com.herokuapp.buyItOrNot.photo.Photo" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<html>
<head>
</head>
<body>
    <a class="no-underline" href="/photo/${photo.id}">
      <img src="${photo.url}" class="img-responsive photo-thumbnail img-thumbnail"
           alt="There was a problem with image. Please, report that issue."/>
    </a>
</body>
</html>