<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ attribute name="user" type="com.herokuapp.buyItOrNot.user.model.User" required="true" %>
<html>
<head>
</head>
<body>
<c:choose>
    <c:when test="${user.name ne null and user.name ne '' and user.lastName ne null and user.lastName ne ''}">
        <c:set var="text" value="${user.name} ${user.lastName}"/>
    </c:when>
    <c:otherwise>
        <c:set var="text" value="${user.accountName}"/>
    </c:otherwise>
</c:choose>


<div class="row">
    <c:choose>
        <c:when test="${user.profilePhotoUrl eq null or user.profilePhotoUrl eq ''}">
            <img src="/resources/images/default_user_icon.jpg" class="miniature-sm"/>
        </c:when>
        <c:otherwise>
            <img src="${user.profilePhotoUrl}" class="miniature-sm"/>
        </c:otherwise>
    </c:choose>

    <c:out value="${text}"/>
</div>
</body>
</html>