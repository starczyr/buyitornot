<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <tags:head title="Credits"/>
</head>
<body>
    <c:set var="me" value="Radek Starczynowski" scope="application"/>

    <div class="jumbotron">
        <h1><fmt:message key="menu.option.credits"/></h1>
        <p><fmt:message key="index.created">
            <fmt:param value="${me}"/>
        </fmt:message></p>
    </div>
    <tags:scripts/>
</body>
</html>