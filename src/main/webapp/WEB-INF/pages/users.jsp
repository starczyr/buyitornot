<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <tags:head title="All userBasicses"/>
</head>
<body>
    <div class="container">
    <tags:menu user="${loggedInUser}"/>

     <c:forEach items="${users}" var="user">
        <c:if test="${user.mail ne loggedInUser.mail}">
            <tags:usercard user="${user}" displayUserName="true"/>
        </c:if>
     </c:forEach>
     </div>
<tags:scripts/>
</body>
</html>