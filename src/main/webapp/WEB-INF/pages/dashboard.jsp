<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<html>
<head>
  <tags:head title="${userExtended.user.name} ${userExtended.user.lastName}"/>
</head>
<body>
<div class="container">

    <tags:menu user="${userExtended.user}"/>

    <%--<div id="observing">--%>
        <%--<c:forEach items="${observedUsers}" var="observedUser">--%>
            <%--<tags:usercard user="${observedUser}" displayUserName="true"/>--%>
        <%--</c:forEach>--%>
    <%--</div>--%>
    <%--<ul style="height: 80px; overflow: auto">--%>
        <%--<li>a</li>--%>
        <%--<li>b</li>--%>
        <%--<li>c</li>--%>
        <%--<li>d</li>--%>
        <%--<li>e</li>--%>
        <%--<li>f</li>--%>
        <%--<li>g</li>--%>
        <%--<li>h</li>--%>
    <%--</ul>--%>

        <c:forEach items="${photos}" var="photo">
          <tags:photo photo="${photo}" displayPhotoAuthor="true"/>
        </c:forEach>




    </div>
  <tags:scripts/>
</body>
</html>