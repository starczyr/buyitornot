<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <tags:head/>
</head>
<body>
<div class="container">
    <tags:menu user="${user}"/>
    <div class="row">
        <h2><fmt:message key="edit.header.update"/></h2>
    </div>
    <div class="row">
        <springform:form method="POST" modelAttribute="user">
            <div class="col-md-5">

                <div class="form-group">
                    <label for="mail"><fmt:message key="login.label.mail"/></label>

                    <springform:input path="mail" id="mail" name="mail" type="text" class="form-control" placeholder="example@mailbox.com"/>
                    <springform:errors path="mail" cssClass="text-danger"/>
                </div>

                <div class="form-group">
                    <label for="name"><fmt:message key="register.label.name"/></label>
                    <springform:input path="name" id="name" name="name" class="form-control"/>
                    <springform:errors path="name"/>
                </div>

                <div class="form-group">
                    <label for="lastName"><fmt:message key="register.label.lastname"/></label>
                    <springform:input path="lastName" id="lastName" name="lastName" class="form-control"/>
                    <springform:errors path="lastName"/>
                </div>

                <div class="form-group">
                    <label for="description"><fmt:message key="edit.label.info"/></label>
                    <springform:textarea path="description" name="description" id="description" cssClass="form-control"/>
                    <springform:errors path="description"/>
                </div>
                </div>
            </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                        <%--TODO: add logic--%>
                        <c:choose>
                            <c:when test="${user.profilePhotoUrl eq null}">
                            <img class="miniature" src="/resources/images/default_user_icon.jpg"/>
                        </c:when>
                            <c:otherwise>
                        <img class="miniature" src="${user.profilePhotoUrl}"/>
                       </c:otherwise>
                        </c:choose>
                    <a onclick="displayUploadSection()" style="vertical-align: bottom; cursor: pointer;">Upload new photo</a>
                    </div>
                    <div id="uploadSection" class="col-md-6 upload-section" hidden>
                        <button role="button" class="btn btn-sm btn-primary" style="position: relative; top: 40px;">Upload</button>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" style="margin-top: 20px"><fmt:message key="edit.button.done"/></button>
              </div>
            </div>
        </springform:form>
    <tags:scripts/>
<script>
    function displayUploadSection() {
        $('#uploadSection').animate({"opacity": "show", top:"0"} , 500);
    }
</script>
</body>
</html>