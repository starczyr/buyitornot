<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <tags:head title="All userBasicses"/>
</head>
<body>
<div class="container">
    <tags:menu user="${loggedInUser}"/>
    <c:set value="0" var="portion"/>

    <c:forEach items="${photos}" var="photo">
        <tags:usercard user="${user}" displayUserName="true"/>
    </c:forEach>

    <button role="button" onclick="load(portion)"></button>
</div>
<tags:scripts/>
<script>
    function load(portion) {

    }
</script>
</body>
</html>