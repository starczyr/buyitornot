<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <tags:head/>
</head>
<body>
    <div class="container">
    <tags:menu user="${loggedInUser}"/>
    <tags:header/>
    <h3 class="text-center">Przeslij</h3>
        <ul class="nav nav-tabs">
            <li><a href="#file" data-toggle="tab"><fmt:message key="filemodal.tab.file"/></a></li>
            <li><a href="#url" data-toggle="tab"><fmt:message key="filemodal.tab.url"/></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="file">
                <form id="fileUploadForm" name="fileForm" method="post" enctype="multipart/form-data">

                        <div class="fileUpload btn btn-primary">
                            <span>Upload</span>
                            <input type="file" name="file" class="upload" onchange="ajaxFilePost('${tempDirName}')"/>
                        </div>

                </form>
                <div id="caches">
                </div>
            </div>
            <div class="tab-pane" id="url">
                <input type="text">
            </div>
        </div>
        <div class="row">
        <img id="loading" class="ajax-loader pull-left" hidden="hidden" src="/resources/images/477.GIF"/>
            <button id="saveButton" type="button"  class="btn btn-primary disabled"
                            onclick="saveCaches()"><fmt:message key="filemodal.button.savechanges"/>
            </button>
        </div>

    </div>
<tags:scripts/>
<script src="/resources/js/uploadSupport.js"></script>
</body>
</html>