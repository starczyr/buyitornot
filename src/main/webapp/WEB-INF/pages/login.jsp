<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <tags:head/>
</head>
<body>
<div class="container">
    <tags:header/>
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <form method="POST" action="/static/j_spring_security_check" role="form">
                <div class="form-group">
                    <label for="mail"><fmt:message key="login.label.mail"/></label>

                    <input id="mail" name="j_username" type="text" class="form-control" placeholder="example@mailbox.com"/>
                </div>
                <div class="form-group">
                    <label for="password"><fmt:message key="login.label.password"/></label>
                    <input id="password" name="j_password" type="password" class="form-control"/>
                </div>

                <div class="checkbox">
                    <label>
                        <fmt:message key="login.label.rememberme"/><input id="remember" name="_spring_security_remember_me" type="checkbox"/>
                    </label>
                </div>
                <button type="submit" class="btn btn-default"><fmt:message key="login.button.login"/></button>
            </form>
        </div>
        <div class="col-md-5">
            <c:if test="${param.failure eq 't'}">
                <div class="well well-lg">
                    <p class="text-warning"><strong>
                        <fmt:message key="login.label.error"/>
                    </strong></p>
                </div>
            </c:if>
        </div>
    </div>
      </div>
    </div>
</div>
  <tags:scripts/>
</body>
</html>