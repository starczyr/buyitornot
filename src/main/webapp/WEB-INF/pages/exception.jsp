<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <tags:head/>
</head>
<body>
    <div class="jumbotron">
        <h2><fmt:message key="exception.header.error"/></h2>
        <p>${message}</p>
    </div>
    <tags:scripts/>
</body>
</html>