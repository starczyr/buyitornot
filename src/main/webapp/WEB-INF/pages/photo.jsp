<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <tags:head/>
</head>
<body>
<div class="container">
    <div class="row text-center">
        <a href="/" class="no-underline"><h1>BuyItOrNot</h1></a>
    </div>
    <div class="row">
        <tags:photo photo="${photo}" displayPhotoAuthor="true"/>
    </div>
</div>
<tags:scripts/>
</body>
</html>
