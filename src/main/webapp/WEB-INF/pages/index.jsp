<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
   <tags:head title="Welcome!"/>
</head>
<body>
  <div class="container">
    <div class="jumbotron">
      <div class="row pull-right">
       <security:authorize var="loggedIn" access="isAuthenticated()"/>
        <c:choose>
            <c:when test="${loggedIn}">
                 <a href="/dashboard" class="btn btn-sm btn-primary"><fmt:message key="index.button.dashboard"/></a>
            </c:when>
            <c:otherwise>
                <a href="/login" class="btn btn-info"><fmt:message key="index.button.signin"/></a>
                <a href="/register" class="btn btn-primary"><fmt:message key="index.button.signup"/></a>
            </c:otherwise>
        </c:choose>
      </div>
        <h1><fmt:message key="index.header"/></h1>
        <p><fmt:message key="index.text.1"/></p>
        <p><fmt:message key="index.text.2"/></p>
        <p><a href="/app/tour" class="btn btn-info btn-small"><fmt:message key="index.button.tour"/></a></p>
    </div>
      <tags:footer><tags:locale/></tags:footer>
  </div>
  <tags:scripts/>
</body>
</html>