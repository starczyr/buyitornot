<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <tags:head/>
</head>
<body>
<div class="container">
    <tags:header/>
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <springform:form method="POST" modelAttribute="user">
                <div class="form-group">
                    <label for="mail"><fmt:message key="login.label.mail"/></label>

                    <springform:input id="mail" path="mail" name="mail" type="text" class="form-control" placeholder="example@mailbox.com"/>
                    <springform:errors path="mail" cssClass="text-danger"/>
                </div>

                <div class="form-group">
                    <label for="password"><fmt:message key="login.label.password"/></label>
                    <springform:input id="password" path="password" name="password" type="password" class="form-control"/>
                    <springform:errors path="password" cssClass="text-danger"/>
                </div>

                <div class="form-group">
                    <label for="repassword"><fmt:message key="register.label.repassword"/></label>
                    <input name="repassword" id="repassword" type="password" class="form-control"/>
                    <c:out value="${repasswordError}"/>
                </div>

                <div class="form-group">
                    <label for="name"><fmt:message key="register.label.name"/></label>
                    <springform:input path="name" id="name" name="name" class="form-control"/>
                    <springform:errors path="name"/>
                </div>


                <div class="form-group">
                    <label for="lastName"><fmt:message key="register.label.lastname"/></label>
                    <springform:input path="lastName" id="lastName" name="lastName" class="form-control"/>
                    <springform:errors path="lastName"/>
                </div>

                <button type="submit" class="btn btn-default"><fmt:message key="register.button.register"/></button>
            </springform:form>
    </div>

</div>
  <tags:scripts/>
</body>
</html>