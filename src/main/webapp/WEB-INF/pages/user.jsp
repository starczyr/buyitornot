<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <tags:head/>
</head>
<body>
<security:authentication property="principal.username" var="mail"/>

  <c:set var="user" value="${userExtended.user}"/>
  <div class="container">
      <tags:menu user="${user}"/>

    <div class="row">
      <div class="col-md-2">
        <tags:usercard user="${user}"/>
      </div>
      <div class="col-md-6">
          <h3>
            <c:choose>
              <c:when test="${user.name eq '' or user.lastName eq ''}">
                ${user.accountName}
              </c:when>
              <c:otherwise>
                ${user.name} ${user.lastName}
              </c:otherwise>
            </c:choose>
            <c:choose>
              <c:when test="${mail eq user.mail}">
                <a href="/${user.accountName}/edit"><span class="glyphicon glyphicon-edit"></span></a>
              </c:when>
              <c:otherwise>

                  <div id="observe">
                  <c:choose>
                      <c:when test="${alreadyObserved eq true}">
                          <span class="time">Observed</span>
                      </c:when>
                      <c:otherwise>
                          <a onclick="observe('${user.accountName}')" class="no-underline">
                              <span class="icon glyphicon glyphicon-plus"></span>
                          </a>
                          <div id="observeResult">
                          </div>
                      </c:otherwise>
                  </c:choose>
                  </div>


              </c:otherwise>
              </c:choose>
          </h3>
          <p>${user.description}</p>
      </div>
    </div>
    <div class="row">
        <ul class="nav nav-tabs">
            <li><a href="#buy" data-toggle="tab" class="active"><fmt:message key="user.tab.buyornot"/></a></li>
            <li><a href="#bought" data-toggle="tab"><fmt:message key="user.tab.bought"/></a></li>
            <li><a href="#notBought" data-toggle="tab"><fmt:message key="user.tab.didntbuy"/></a></li>
        </ul>

        <div class="tab-content">
            <div class="active tab-pane" id="buy">
                <c:forEach items="${buyOrNotPhotos}" var="photo">
                    <tags:photo photo="${photo}"/>
                </c:forEach>
            </div>

            <div class="tab-pane" id="bought">
                <c:forEach items="${boughtPhotos}" var="photo">
                    <tags:thumbnail photo="${photo}"/>
                </c:forEach>
            </div>
            <div class="tab-pane" id="notBought">
                <div class="row">
                <c:forEach items="${didntBuyPhotos}" var="photo">
                    <tags:thumbnail photo="${photo}"/>
                </c:forEach>
                    </div>
            </div>
        </div>
    </div>
  </div>
  <tags:scripts/>
<script>

    function observe(accountName) {
        var urlString = '/' + accountName + '/observe';
        $.ajax({
            url: urlString,
            type: 'GET'
        }).done(function(html) {
            $("#observeResult").append(html);
        }).fail(function() {
            alert('failure');
        }).always(function(html) {
        });
    }
</script>
</body>
</html>