package com.herokuapp.buyItOrNot.user;

import com.herokuapp.buyItOrNot.photo.Photo;
import com.herokuapp.buyItOrNot.photo.PhotoComparator;
import com.herokuapp.buyItOrNot.photo.PhotoService;
import com.herokuapp.buyItOrNot.serviceUtils.LoginUtils;
import com.herokuapp.buyItOrNot.serviceUtils.RegisterationDisabled;
import com.herokuapp.buyItOrNot.serviceUtils.TimeCounter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.user.model.UserExtended;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * User: Radek Starczynowski
 * Date: 01.12.13
 * Time: 23:05
 */
@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private LoginUtils loginUtils;
	@Autowired
	private PhotoService photoService;

	public UserController() {}





//	@ModelAttribute("timeCounter")
//	public TimeCounter getTimeCounter() {
//		return new TimeCounter();
//	}

	@PreAuthorize("@loginUtils.isUser()")
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView displayUser() {
		String mail = loginUtils.getLoggedInUserMail();
		UserExtended userExtended = userService.getUserExtendedWithItemsByMail(mail, new UserFetcher()
                .fetch(UserFetcher.Item.PHOTOS, UserFetcher.Item.OBSERVED_USERS));

		ModelAndView modelAndView = new ModelAndView();
		List<User> users = userExtended.getObservedList().getObserved();

		List<Photo> photos = getPhotosToDisplay(users);
		photos.addAll(getPhotosToDisplay(userExtended));

		sortByAddedOn(photos);

		modelAndView.addObject("userExtended", userExtended);

		modelAndView.addObject("photos", photos);
//		modelAndView.addObject("observedUsers", users);

		modelAndView.setViewName("dashboard");

		return modelAndView;
	}

	public void sortByAddedOn(List<Photo> photos) {
		Collections.sort(photos, new PhotoComparator());
	}

	public List<Photo> getPhotosToDisplay(UserExtended userExtended) {
		return photoService.getStatusPhotos(userExtended, Photo.Status.BUY_OR_NOT);
	}

	public List<Photo> getPhotosToDisplay(List<User> users) {
		List<Photo> photosToDisplay = new ArrayList<Photo>();

		for(User user : users) {
			// TODO: get photos by portions and load them with ajax
			photosToDisplay.addAll(photoService.getPhotos(user, Photo.Status.BUY_OR_NOT));
		}
		return photosToDisplay;
	}



	// TODO: finish pre-authorize
	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{accountName}", method = RequestMethod.GET)
	public ModelAndView getUser(@PathVariable String accountName) {
		ModelAndView modelAndView = new ModelAndView();
		UserExtended userExtended = userService.getUserExtendedWithItemsByAccountName(accountName, new UserFetcher()
                .fetch(UserFetcher.Item.COMMENTS)
                .fetch(UserFetcher.Item.PHOTOS)
                .fetch(UserFetcher.Item.OBSERVED_USERS)
                .fetch(UserFetcher.Item.VOTES_FOR_COMMENTS)
                .fetch(UserFetcher.Item.VOTES_FOR_PHOTOS));

		if (userExtended == null) {
			throw new NoSuchUserException();
		}
		List<Photo> buyOrNot, bought, didntBuy;

		buyOrNot = photoService.getStatusPhotos(userExtended, Photo.Status.BUY_OR_NOT);
		bought = photoService.getStatusPhotos(userExtended, Photo.Status.BOUGHT);
		didntBuy = photoService.getStatusPhotos(userExtended, Photo.Status.DIDNT_BUY);

		boolean isObserved = userService.getUserExtendedWithItemsByMail(loginUtils.getLoggedInUserMail(), new UserFetcher()
                .fetch(UserFetcher.Item.OBSERVED_USERS))
				.getObservedList()
				.getObserved()
				.contains(userExtended.getUser());

		modelAndView.addObject("userExtended", userExtended);

        // TODO: separate them in view (because of json model)
		modelAndView.addObject("buyOrNotPhotos", buyOrNot);
		modelAndView.addObject("boughtPhotos", bought);
		modelAndView.addObject("didntBuyPhotos", didntBuy);
		modelAndView.addObject("alreadyObserved", isObserved);
        //

		modelAndView.setViewName("user");

		return modelAndView;
	}


	// TODO: finish pre-authorize
	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{accountName}/edit", method = RequestMethod.GET)
	public ModelAndView getUserEditForm(@PathVariable String accountName) {
		ModelAndView modelAndView = new ModelAndView();

		User user = userService.getUserByAccountName(accountName);
		if (user == null) {
			throw new NoSuchUserException();
		}
		modelAndView.addObject("user", user);
		modelAndView.setViewName("edit");

		return modelAndView;
	}

	// TODO: finish pre-authorize
	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{accountName}/edit", method = RequestMethod.POST)
	public ModelAndView editUserInfo(@PathVariable String accountName, @Valid User user, BindingResult bindingResult) {
		User userInDB = userService.getUserByAccountName(accountName);

		// TODO: validate info

		userInDB.setMail(user.getMail());
		userInDB.setName(user.getName());
		userInDB.setLastName(user.getLastName());
		userInDB.setDescription(user.getDescription());

		ModelAndView modelAndView = new ModelAndView();
		userService.update(userInDB);

		modelAndView.addObject("user", userInDB);
		modelAndView.setViewName("redirect:/" + accountName);

		return modelAndView;
	}

	// TODO: finish pre-authorize
	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{accountName}/observe", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String addFriend(@PathVariable String accountName) {
		User userInDB = userService.getUserByAccountName(accountName);
		
		String mail = loginUtils.getLoggedInUserMail();
		UserExtended loggedIn = userService.getUserExtendedWithItemsByMail(mail,
                new UserFetcher().fetch(UserFetcher.Item.OBSERVED_USERS));

		ObservedList observedList = loggedIn.getObservedList();
		List<User> observed = observedList.getObserved();
		if(observed.contains(userInDB)) {
			return "already observing";
		}
		observed.add(userInDB);
		
		userService.update(loggedIn);

		return "ok";
	}

	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/allusers", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	ModelAndView getUsers() {
		ModelAndView modelAndView = new ModelAndView();
		User loggedInUser = userService.getUserByMail(loginUtils.getLoggedInUserMail());

		List<User> users = userService.getUsers(0, 10);
		modelAndView.addObject("users", users);
		modelAndView.addObject("user", loggedInUser);
		modelAndView.setViewName("users");

		return modelAndView;
	}

	@ExceptionHandler(NoSuchUserException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ModelAndView processNoSuchUserException() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("message", "Sorry, no such user exists :-(");
		modelAndView.setViewName("exception");

		return modelAndView;
	}

	@ExceptionHandler(RegisterationDisabled.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ModelAndView processException() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("message", "Sorry, registration is now disabled by admin");
		modelAndView.setViewName("exception");

		return modelAndView;
	}
}
