package com.herokuapp.buyItOrNot.user.model;

import com.herokuapp.buyItOrNot.comment.Comment;
import com.herokuapp.buyItOrNot.photo.Photo;
import com.herokuapp.buyItOrNot.user.ObservedList;
import com.herokuapp.buyItOrNot.vote.VoteComment;
import com.herokuapp.buyItOrNot.vote.VotePhoto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * User: Radek Starczynowski
 * Date: 29.11.13
 * Time: 19:04
 */
@Entity
@Table(name = "public.user")
public class User {
	@Id
	@Column(name = "id_user", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "mail", unique = true, nullable = false)
	private String mail;

	@Column(name = "password")
	private String password;

	@Column(name = "account_creation_date", nullable = false)
	private Date accountCreationDate;

	@Column(name = "last_login_date")
	private Date lastLoginDate;

	@Column(name = "role", nullable = false)
	private String role;

	@Column(name = "account_name", unique = true, nullable = false)
	private String accountName;

	@Column(name = "active", nullable = false)
	private boolean active;

	@Column(name = "description")
	private String description;

	@Column(name = "profile_photo_url")
	private String profilePhotoUrl;

//    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
//    @OrderBy(value = "added_on DESC")
//    private List<Comment> comments;
//
//    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @OrderBy(value = "added_on DESC")
//    private List<Photo> photos;
//
//    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY,
//            cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
//    private List<VotePhoto> votesForPhotos;
//
//    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY,
//            cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
//    private List<VoteComment> votesForComments;
//
//    @OneToOne(cascade=CascadeType.ALL)
//    @PrimaryKeyJoinColumn
//    private ObservedList observedList;


//    public static class UserBuilder {
//        private String name;
//        private String lastName;
//        private String mail;
//        private String password;
//        private Date accountCreationDate;
//        private Date lastLoginDate;
//        private String role;
//        private String accountName;
//        private boolean active;
//        private String description;
//        private String profilePhotoUrl;
//
//
//        public UserBuilder() {
//        }
//
//        public User build() {
//            User user = new User();
//            user.setName(name);
//            user.setLastName(lastName);
//            user.setMail(mail);
//            user.setPassword(password);
//            user.setAccountCreationDate(accountCreationDate);
//            user.setLastLoginDate(lastLoginDate);
//            user.setRole(role);
//            user.setAccountName(accountName);
//            user.setActive(active);
//            user.setDescription(description);
//            user.setProfilePhotoUrl(profilePhotoUrl);
//
//            return user;
//        }
//
//        public UserBuilder setName(String name) {
//            this.name = name;
//            return this;
//        }
//
//        public UserBuilder setLastName(String lastName) {
//            this.lastName = lastName;
//            return this;
//        }
//
//        public UserBuilder setMail(String mail) {
//            this.mail = mail;
//            return this;
//        }
//
//        public UserBuilder setPassword(String password) {
//            this.password = password;
//            return this;
//        }
//
//        public UserBuilder setAccountCreationDate(Date accountCreationDate) {
//            this.accountCreationDate = accountCreationDate;
//            return this;
//        }
//
//        public UserBuilder setLastLoginDate(Date lastLoginDate) {
//            this.lastLoginDate = lastLoginDate;
//            return this;
//        }
//
//        public UserBuilder setRole(String role) {
//            this.role = role;
//            return this;
//        }
//
//        public UserBuilder setAccountName(String accountName) {
//            this.accountName = accountName;
//            return this;
//        }
//
//        public UserBuilder setActive(boolean active) {
//            this.active = active;
//            return this;
//        }
//
//        public UserBuilder setDescription(String description) {
//            this.description = description;
//            return this;
//        }
//
//        public UserBuilder setProfilePhotoUrl(String profilePhotoUrl) {
//            this.profilePhotoUrl = profilePhotoUrl;
//            return this;
//        }
//    }

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getAccountCreationDate() {
		return accountCreationDate;
	}

	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProfilePhotoUrl() {
		return profilePhotoUrl;
	}

	public void setProfilePhotoUrl(String profilePhotoUrl) {
		this.profilePhotoUrl = profilePhotoUrl;
	}

//
//    public List<Comment> getComments() {
//        return comments;
//    }
//
//    public void setComments(List<Comment> comments) {
//        this.comments = comments;
//    }
//
//    public List<Photo> getPhotos() {
//        return photos;
//    }
//
//    public void setPhotos(List<Photo> photos) {
//        this.photos = photos;
//    }
//
//    public List<VotePhoto> getVotesForPhotos() {
//        return votesForPhotos;
//    }
//
//    public void setVotesForPhotos(List<VotePhoto> votesForPhotos) {
//        this.votesForPhotos = votesForPhotos;
//    }
//
//    public List<VoteComment> getVotesForComments() {
//        return votesForComments;
//    }
//
//    public void setVotesForComments(List<VoteComment> votesForComments) {
//        this.votesForComments = votesForComments;
//    }
//
//    public ObservedList getObservedList() {
//        return observedList;
//    }
//
//    public void setObservedList(ObservedList observedList) {
//        this.observedList = observedList;
//    }

    @Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("User{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", accountCreationDate=").append(accountCreationDate);
		sb.append(", lastLoginDate=").append(lastLoginDate);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if(active != user.active) return false;
		if(!accountCreationDate.equals(user.accountCreationDate)) return false;
		if(!accountName.equals(user.accountName)) return false;
		if(!id.equals(user.id)) return false;
		if(lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
		if(!mail.equals(user.mail)) return false;
		if(name != null ? !name.equals(user.name) : user.name != null) return false;
		if(!role.equals(user.role)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + mail.hashCode();
		result = 31 * result + accountCreationDate.hashCode();
		result = 31 * result + role.hashCode();
		result = 31 * result + accountName.hashCode();
		result = 31 * result + (active ? 1 : 0);
		return result;
	}
}
