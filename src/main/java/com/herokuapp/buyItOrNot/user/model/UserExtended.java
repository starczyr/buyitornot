package com.herokuapp.buyItOrNot.user.model;

import com.herokuapp.buyItOrNot.comment.Comment;
import com.herokuapp.buyItOrNot.photo.Photo;
import com.herokuapp.buyItOrNot.user.ObservedList;
import com.herokuapp.buyItOrNot.vote.VoteComment;
import com.herokuapp.buyItOrNot.vote.VotePhoto;

import javax.persistence.*;
import java.util.List;

/**
* Radek Starczynowski
* <radek.starczynowski@gmail.com>
* 31.01.14, 14:57
*/
@Entity
@Table(name = "public.user_extended")
public class UserExtended {
    @Id
    @Column(name = "id_user_extended", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private User user;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    @OrderBy(value = "added_on DESC")
    private List<Comment> comments;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy(value = "added_on DESC")
    private List<Photo> photos;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY,
            cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
    private List<VotePhoto> votesForPhotos;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY,
            cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
    private List<VoteComment> votesForComments;

    @OneToOne(cascade=CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ObservedList observedList;

    public UserExtended() {
    }

    public UserExtended(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public List<VotePhoto> getVotesForPhotos() {
        return votesForPhotos;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setVotesForPhotos(List<VotePhoto> votesForPhotos) {
        this.votesForPhotos = votesForPhotos;
    }

    public List<VoteComment> getVotesForComments() {
        return votesForComments;
    }

    public void setVotesForComments(List<VoteComment> votesForComments) {
        this.votesForComments = votesForComments;
    }

    public ObservedList getObservedList() {
        return observedList;
    }

    public void setObservedList(ObservedList observedList) {
        this.observedList = observedList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserExtended that = (UserExtended) o;

        if (!id.equals(that.id)) return false;
        if (!user.equals(that.user)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + user.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserExtended{");
        sb.append("id=").append(id);
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
