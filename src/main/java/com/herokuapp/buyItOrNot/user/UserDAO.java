package com.herokuapp.buyItOrNot.user;

import com.herokuapp.buyItOrNot.dao.DAO;

import java.util.ArrayList;
import java.util.List;

import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.user.model.UserExtended;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * User: Radek Starczynowski
 * Date: 29.11.13
 * Time: 19:39
 */
@Repository
public class UserDAO implements DAO<User> {
	private SessionFactory sessionFactory;

	@Autowired
	public UserDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public User getById(Long id) {
		return (User) getSession().load(User.class, id);
	}

	@Override
	public void delete(User user) {
		getSession().delete(user);
	}

	@Override
	public void update(User user) {
		getSession().update(user);
	}


    public void update(UserExtended userExtended) {
        getSession().update(userExtended);
    }

    @Override
    public void save(User user) {
        getSession().save(user);
    }

    public void save(UserExtended userExtended) {
        getSession().save(userExtended);
    }

	@SuppressWarnings("unchecked")
	public List<User> getAll() {
		return getSession().createQuery(
			"from com.herokuapp.buyItOrNot.user.model.User").list();
	}

	public User getUserByMail(String mail) {
		return (User) getSession().createQuery(
			"from com.herokuapp.buyItOrNot.user.model.User as user " +
				"where user.mail=:mail"
			).setParameter("mail", mail).uniqueResult();
	}

	public User getUserByAccountName(String accountName) {
		return (User) getSession().createQuery(
			"from com.herokuapp.buyItOrNot.user.model.User as user " +
				"where user.accountName=:accountName"
			).setParameter("accountName", accountName).uniqueResult();
	}

    public UserExtended getUserExtendedByAccountName(String accountName) {
        return (UserExtended) getSession().createQuery("from com.herokuapp.buyItOrNot.user.model.UserExtended as extended " +
                "where extended.user.accountName = :accountName").setParameter("accountName", accountName).uniqueResult();
    }
    public UserExtended getUserExtendedByMail(String mail) {
        return (UserExtended) getSession().createQuery("from com.herokuapp.buyItOrNot.user.model.UserExtended as extended " +
                "" +
                "where extended.user.mail = :mail").setParameter("mail", mail).uniqueResult();
    }

    public UserExtended getExtendedForUser(User user) {
         UserExtended result = (UserExtended) getSession().createQuery("from com.herokuapp.buyItOrNot.user.model.UserExtended as extended " +
                "" +
                "where extended.user.id = :userId").setParameter("userId", user.getId()).uniqueResult();
        result.setUser(user);

        return result;
    }

	/**
	 * Fetch only required number of users from db
	 * @param offset from which row to start
	 * @param howMany just how many rows to fetch
	 * @return users
	 */
	public List<User> getUsers(int offset, int howMany) {
		ScrollableResults scrollable = getSession().createQuery(
			"from com.herokuapp.buyItOrNot.user.model.User as user " +
				"order by user.lastLoginDate desc, user.accountCreationDate desc"
			).scroll();
		List<User> users = new ArrayList<User>();

		if (!scrollable.setRowNumber(offset)) {
			throw new RuntimeException("Wrong row index");
		}

		int counter = howMany;
		do {
			users.add((User) scrollable.get(0));
			counter--;
		} while (scrollable.next() && counter > 0);
		scrollable.close();

		return users;
	}

    public List<User> getUsersLike(String string) {
        throw new NotYetImplementedException();
    }
}
