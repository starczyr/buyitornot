package com.herokuapp.buyItOrNot.user;

import com.herokuapp.buyItOrNot.user.model.User;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


/**
 * User: Radek Starczynowski
 * Date: 23.12.13
 * Time: 15:02
 */
@Entity
@Table(name = "public.observed_list")
public class ObservedList {
	@Id
	@Column(name = "id_observed_list", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "observed_users", joinColumns = @JoinColumn(name = "id_observed_list"),
		inverseJoinColumns = @JoinColumn(name = "id_user"))
	private List<User> observed;

	public ObservedList() {
		this.observed = new ArrayList<User>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<User> getObserved() {
		return observed;
	}

	public void setObserved(List<User> observed) {
		this.observed = observed;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;

		ObservedList that = (ObservedList) o;

		if(!id.equals(that.id)) return false;
		if(observed != null ? !observed.equals(that.observed) : that.observed != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + (observed != null ? observed.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ObservedList{");
		sb.append("id=").append(id);
		sb.append(", observed=").append(observed);
		sb.append('}');
		return sb.toString();
	}
}
