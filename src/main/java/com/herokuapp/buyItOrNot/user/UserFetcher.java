package com.herokuapp.buyItOrNot.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.hibernate.Hibernate;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 02.01.14 21:02
 */
public class UserFetcher {
	public static enum Item {
		PHOTOS,
		COMMENTS,
		VOTES_FOR_COMMENTS,
		VOTES_FOR_PHOTOS,
		OBSERVED_USERS
	}
	private List<Item> items;
	
	public UserFetcher() {
		items = new ArrayList<Item>();
	}

	public UserFetcher fetch(Item ... items) {
        Collections.addAll(this.items, items);

        return this;
	}
	
	public List<Item> getItems() {
		return items;
	}

}
