package com.herokuapp.buyItOrNot.user;

import java.util.List;

import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.user.model.UserExtended;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: Radek Starczynowski
 * Date: 29.11.13
 * Time: 19:44
 */
@Transactional(readOnly = true)
@Service
public class UserService {
	private UserDAO userDAO;

	public UserService() {}

	@Autowired
	public UserService(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public User getById(Long id) {
		return userDAO.getById(id);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void save(User user) {
		userDAO.save(user);
	}

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void save(UserExtended userExtended) {
        userDAO.save(userExtended);
    }

	@Transactional(readOnly = false)
	public void delete(User user) {
		userDAO.delete(user);
	}

	@Transactional(readOnly = false)
	public void update(User user) {
		userDAO.update(user);
	}

    @Transactional(readOnly = false)
    public void update(UserExtended userExtended) {
        userDAO.update(userExtended);
    }

	public List<User> getAll() {
		return userDAO.getAll();
	}

	public User getUserByMail(String mail) {
		return userDAO.getUserByMail(mail);
	}

	public User getUserByAccountName(String accountName) {
		return userDAO.getUserByAccountName(accountName);
	}

	public List<User> getUsers(int offset, int howMany) {
		return userDAO.getUsers(offset, howMany);
	}

//    public List<User> getUsersExtendedWith(int offset, int howMany, UserFetcher userFetcher) {
//        List<User> users = getUsers(offset, howMany);
//        for(User user : users) {
//            initialize(user, userFetcher);
//        }
//        return users;
//    }

	public UserExtended getUserExtendedWithItemsByMail(String mail, UserFetcher userFetcher) {
		UserExtended userExtended = userDAO.getUserExtendedByMail(mail);
		if(userExtended != null) {
            initialize(userExtended, userFetcher);
        } else {
            throw new NoSuchUserException();
        }
		return userExtended;
	}

	public UserExtended getUserExtendedWithItemsByAccountName(String accountName, UserFetcher userFetcher) {
		UserExtended userExtended = userDAO.getUserExtendedByAccountName(accountName);
	    if(userExtended != null) {
            initialize(userExtended, userFetcher);
        } else {
            throw new NoSuchUserException();
        }
		return userExtended;
	}




	@Transactional(propagation = Propagation.SUPPORTS)
	public void initialize(UserExtended userExtended, UserFetcher userFetcher) {
		for(UserFetcher.Item item : UserFetcher.Item.values()) {
			if(userFetcher.getItems().contains(item)) {
				initialize(item, userExtended);
			}
		}
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private void initialize(UserFetcher.Item item, UserExtended user) {
		switch(item) {
			case COMMENTS:
				Hibernate.initialize(user.getComments());
				break;
			case PHOTOS:
				Hibernate.initialize(user.getPhotos());
				break;
			case VOTES_FOR_COMMENTS:
				Hibernate.initialize(user.getVotesForComments());
				break;
			case VOTES_FOR_PHOTOS:
				Hibernate.initialize(user.getVotesForPhotos());
				break;
			case OBSERVED_USERS:
				Hibernate.initialize(user.getObservedList());
				ObservedList list = user.getObservedList();
				if(list != null) {
					Hibernate.initialize(list.getObserved());
				}
				break;
			default:
				break;
		}
	}

    public List<User> getUsersLike(String string) {
        return userDAO.getUsersLike(string);
    }
}
