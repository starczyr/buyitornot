package com.herokuapp.buyItOrNot.comment;

import com.herokuapp.buyItOrNot.photo.Photo;
import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.vote.VoteComment;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * User: Radek Starczynowski
 * Date: 13.12.13
 * Time: 23:34
 */
@Entity
@Table(name = "public.comment")
public class Comment {
	@Id
	@Column(name = "id_comment", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_photo", nullable = false)
	private Photo photo;

	@Column(name = "content", nullable = false)
	private String content;

	@Column(name = "added_on", nullable = false)
	private Date addedOn;

	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.PERSIST})
	@JoinColumn(name = "id_user", nullable = false)
	private User author;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "voted", fetch = FetchType.EAGER)
	private Set<VoteComment> votes;

	public Comment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Set<VoteComment> getVotes() {
		return votes;
	}

	public void setVotes(Set<VoteComment> votes) {
		this.votes = votes;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;

		Comment comment = (Comment) o;

		if(addedOn != null ? !addedOn.equals(comment.addedOn) : comment.addedOn != null) return false;
		if(!author.equals(comment.author)) return false;
		if(content != null ? !content.equals(comment.content) : comment.content != null) return false;
		if(!id.equals(comment.id)) return false;
		if(photo != null ? !photo.equals(comment.photo) : comment.photo != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + (photo != null ? photo.hashCode() : 0);
		result = 31 * result + (content != null ? content.hashCode() : 0);
		result = 31 * result + (addedOn != null ? addedOn.hashCode() : 0);
		result = 31 * result + author.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Comment{");
		sb.append("id=").append(id);
		sb.append(", photo=").append(photo);
		sb.append(", content='").append(content).append('\'');
		sb.append(", addedOn=").append(addedOn);
		sb.append(", author=").append(author);
		sb.append('}');
		return sb.toString();
	}



}
