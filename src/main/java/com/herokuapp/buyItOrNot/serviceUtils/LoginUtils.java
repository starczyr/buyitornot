package com.herokuapp.buyItOrNot.serviceUtils;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * User: Radek Starczynowski
 * Date: 07.12.13
 * Time: 23:44
 */
@Service
public class LoginUtils {
	public final String ADMIN = "ROLE_ADMIN";
	public final String USER = "ROLE_USER";
	public final String ANONYMOUS = "ROLE_ANONYMOUS";

	public LoginUtils() {
	}

	private Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public String getLoggedInUserMail() {
		return getAuthentication().getName();
	}

	public boolean hasRole(String role) {
		Collection<? extends GrantedAuthority> authorities = getAuthentication().getAuthorities();
		for(GrantedAuthority authority : authorities) {
			if(authority.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	public boolean isUser() {
		return hasRole(USER);
	}

	public boolean isAdmin() {
		return hasRole(ADMIN);
	}

	public boolean isAnonymous() {
		return hasRole(ANONYMOUS);
	}
}
