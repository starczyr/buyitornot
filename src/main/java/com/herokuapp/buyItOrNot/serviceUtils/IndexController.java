package com.herokuapp.buyItOrNot.serviceUtils;

import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 06.01.14 01:21
 */
@Controller
public class IndexController {
	@Autowired
	private LoginUtils loginUtils;
	@Autowired
	private SessionLocaleResolver localeResolver;

	public IndexController() {
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView displayIndex(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView();
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals("language")) {
                localeResolver.setLocale(request, response, new Locale(cookie.getValue()));
            }
        }

		if (loginUtils.isAnonymous()) {
			modelAndView.setViewName("index");
		} else {
			modelAndView.setViewName("redirect:/dashboard");
		}

		return modelAndView;
	}
}
