package com.herokuapp.buyItOrNot.serviceUtils;

import java.text.DateFormat;
import java.util.Date;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 06.01.14 17:04
 */
public class TimeCounter {
	private static final Long MILIS_IN_SECOND = 1000L;
	private static final Long MILIS_IN_MINUTE = MILIS_IN_SECOND * 60;
	private static final Long MILIS_IN_HOUR = MILIS_IN_MINUTE * 60;
	private static final Long MILIS_IN_DAY = MILIS_IN_HOUR * 24;


	public String countTimeSince(Date date) {
		Date now = new Date();
		Long gap = now.getTime() - date.getTime();
		Long seconds = 1000 % gap;

		if(gap / MILIS_IN_DAY > 7) {
			return DateFormat.getDateTimeInstance().format(date);
		} else if(gap / MILIS_IN_DAY > 0) {
			return gap / MILIS_IN_DAY + " days ago";
		} else if(gap / MILIS_IN_HOUR > 0) {
			return gap / MILIS_IN_HOUR + " hours ago";
		} else if(gap / MILIS_IN_MINUTE > 0) {
			return gap / MILIS_IN_MINUTE + " minutes ago";
		} else if(gap / MILIS_IN_SECOND > 0) {
			return gap / MILIS_IN_SECOND + " seconds ago";
		} else {
			return "now";
		}
	}
}
