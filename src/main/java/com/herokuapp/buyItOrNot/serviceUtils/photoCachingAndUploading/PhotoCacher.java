package com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading;

import com.herokuapp.buyItOrNot.serviceUtils.RandomStringsGenerator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

/**
 * User: Radek Starczynowski
 * Date: 25.12.13
 * Time: 15:48
 */
public class PhotoCacher {
	private static Map<String, PhotoCache> photos = new HashMap<String, PhotoCache>();
	
	public static String cachePhoto(MultipartFile multipartFile, File tempDir) throws IOException {
		String name = RandomStringsGenerator.createRandomString(20);
		File photo = new File(tempDir, name);

		multipartFile.transferTo(photo);

		PhotoCache cache = new PhotoCache(multipartFile.getOriginalFilename(),
				multipartFile.getContentType(),
				photo);

		photos.put(name, cache);

		return name;
	}

	public static PhotoCache getCache(String cacheName) {
		return photos.get(cacheName);
	}

    public static void deleteCache(String cacheName) {
        photos.get(cacheName).deleteTempFile();
        photos.remove(cacheName);
    }
}
