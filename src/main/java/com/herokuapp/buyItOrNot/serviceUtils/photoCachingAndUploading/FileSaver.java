package com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading;

import com.herokuapp.buyItOrNot.serviceUtils.RandomStringsGenerator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.web.multipart.MultipartFile;

/**
 * User: Radek Starczynowski
 * Date: 17.12.13
 * Time: 15:44
 */
public class FileSaver {
	public static File saveFile(PhotoCache cache, File folderToSave) throws FileUploadException, IOException {
		String extension = cache.getContentType().replace("image/", ".");
		File photo = save(folderToSave, extension);

		cache.transferTo(photo);
		cache.deleteTempFile();

		return photo;
	}

	public static File saveFile(MultipartFile file, File folderToSave) throws FileUploadException, IOException {
		String extension = file.getContentType().replace("image/", ".");
		File photo = save(folderToSave, extension);

		file.transferTo(photo);

		return photo;
	}

	private static File save(File folderToSave, String extension) {
		String hashedFilename = RandomStringsGenerator.createRandomString(20);

		String path = folderToSave.getAbsolutePath() + '/' + hashedFilename;

		String url;
		File photo;
		do {
			url = path + RandomStringsGenerator.createRandomString(6) + '.' + extension;
			photo = new File(url);
		} while (photo.exists());

		return photo;
	}
}
