package com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading;

import com.herokuapp.buyItOrNot.serviceUtils.RandomStringsGenerator;
import com.herokuapp.buyItOrNot.user.model.User;

import java.io.File;
import java.io.IOException;

import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 25.12.13 23:05
 */
@Service
public class PhotosDirHolder {
	private String photosDirRealPath;
	private String photosContextPath = "/resources/photos";
	private File photosDir;
	private File tempDir;
	private static int instances = 0;

	@Autowired
	public PhotosDirHolder(ServletContext servletContext) throws IOException {
		instances++;
		this.photosDirRealPath = servletContext.getRealPath(photosContextPath);
		photosDir = new File(this.photosDirRealPath);

		if (!photosDir.exists()) {
			if (!photosDir.mkdir()) {
				throw new RuntimeException("Couldn't create photos dir");
			}
		}

		String tempDirName = createTempDirName();
		this.tempDir = createTempDir(tempDirName);

		System.err.println("instances: " + instances);
	}

	@SuppressWarnings("all")
	@PreDestroy
	public void deleteTempDir() {
		if (tempDir.exists()) {
			for(File file : tempDir.listFiles()) {
				file.delete();
			}
			if (!tempDir.delete()) {
				System.err.println("Delete temp dir: fail");
			} else {
				System.err.println("Delete temp dir: success");
			}
		}
	}

	public File getUserFolder(User user) throws FileUploadException {
		if (photosDir.isDirectory()) {
			File userDir = new File(photosDir, String.valueOf(user.getId()));
			boolean result;
			if (!userDir.exists()) {
				result = userDir.mkdir();
				if (!result) {
					throw new FileUploadException("Can't create folder for user");
				}
			}
			return userDir;
		}
		throw new FileUploadException("Photos folder doesn't exist");
	}

	public String convertSavedPhotoRealtoContextPath(String realPath) {
		return photosContextPath + realPath.replace(photosDirRealPath, StringUtils.EMPTY);
	}

	public File getTempDir() {
		return tempDir;
	}

	public File getPhotosDir() {
		return photosDir;
	}

	public String getPhotosDirRealPath() {
		return photosDirRealPath;
	}

	public String getPhotosContextPath() {
		return photosContextPath;
	}

	private String createTempDirName() {
		StringBuilder builder = new StringBuilder("temp_dir_");
		builder.append(RandomStringsGenerator.createRandomString(20));

		return builder.toString();
	}

	private File createTempDir(String tempDirName) {
		String url = photosDirRealPath + "/" + tempDirName;
		File temp = new File(url);

		if (!temp.mkdir()) {
			throw new RuntimeException("Couldn't create temp dir");
		}

		return temp;
	}
}
