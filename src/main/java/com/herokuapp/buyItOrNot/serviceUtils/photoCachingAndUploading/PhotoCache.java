package com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 26.12.13 00:28
 */
public class PhotoCache {
	private static final String ALLOWED_FORMAT_PATTERNS = "(png|jpg|jpeg)";
	private File photo;
	private String originalName;
	private String contentType;

	public PhotoCache(String originalName, String contentType, File photo) {
		this.originalName = originalName;
		this.contentType = contentType.replace("image/", "");
		this.photo = photo;
		System.err.println("PhotoCache:photo content type: " + contentType);
		if (!this.contentType.matches(ALLOWED_FORMAT_PATTERNS)) {
			throw new RuntimeException("Wrong file format");
		}
	}

	public void deleteTempFile() {
		if (photo.exists()) {
			boolean result = photo.delete();
			if (!result) {
				throw new RuntimeException("Couldn't delete cached photo");
			}
		}
	}

	/**
	 * Try to delete manually but also call delete when deleting
	 * photo cache object
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		deleteTempFile();
	}

	public File getPhoto() {
		return photo;
	}

	public String getOriginalName() {
		return originalName;
	}

	public String getContentType() {
		return contentType;
	}

	public void transferTo(File photo) throws RuntimeException {
		if(this.photo == null || photo == null) {
			throw new RuntimeException("Null photo");
		}

		FileChannel cacheChannel = null, photoChannel = null;
		try {
			if(!photo.exists()) {
				boolean result = photo.createNewFile();
                if(!result) {
                    throw new RuntimeException("Problem with saving file");
                }
			}
			cacheChannel = new FileInputStream(this.photo).getChannel();
			photoChannel = new FileOutputStream(photo).getChannel();

			cacheChannel.transferTo(0, cacheChannel.size(), photoChannel);
		} catch (IOException e) {
			throw new RuntimeException("Problem while saving file: " + e.getMessage());
		} finally {
			try {
				if (cacheChannel != null) {
					cacheChannel.close();
				}
				if (photoChannel != null) photoChannel.close();
			} catch (IOException e) {
				throw new RuntimeException("Problem while saving file");
			}
		}

	}

	public String getCacheName() {
		return photo.getName();
	}

}
