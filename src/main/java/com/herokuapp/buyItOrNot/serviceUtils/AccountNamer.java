package com.herokuapp.buyItOrNot.serviceUtils;

import com.herokuapp.buyItOrNot.user.model.User;

/**
 * User: Radek Starczynowski
 * Date: 11.12.13
 * Time: 21:50
 */
public class AccountNamer {
	private static String EMPTY = "";

	public static String generateAccountName(User user) {
		if(user.getName() == null || user.getName().equals(EMPTY) ||
		   user.getLastName() == null || user.getLastName().equals(EMPTY)) {
			return getNameFromMail(user.getMail());
		}
		StringBuilder builder = new StringBuilder(user.getName().toLowerCase());
		builder.append(user.getLastName().toLowerCase());

		return builder.toString();
	}

	private static String getNameFromMail(String mail) {
		String[] beforeAt = mail.split("@");
		String[] pieces = beforeAt[0].split("\\W");

		StringBuilder builder = new StringBuilder();

		for(String piece : pieces) {
			if(piece.matches("\\w+")) {
				builder.append(piece.toLowerCase());
			}
		}
		return builder.toString();
	}
}
