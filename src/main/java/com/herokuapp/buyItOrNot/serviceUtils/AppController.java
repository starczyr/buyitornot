package com.herokuapp.buyItOrNot.serviceUtils;

import com.herokuapp.buyItOrNot.comment.Comment;
import com.herokuapp.buyItOrNot.crypto.Crypto;
import com.herokuapp.buyItOrNot.photo.Photo;
import com.herokuapp.buyItOrNot.user.ObservedList;
import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.herokuapp.buyItOrNot.user.model.UserExtended;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * User: Radek Starczynowski
 * Date: 23.12.13
 * Time: 01:04
 */
@Controller
public class AppController {
	@Autowired
	private UserService userService;
	@Autowired
	private LoginUtils loginUtils;
	@Autowired
	private SessionLocaleResolver localeResolver;


	public AppController() {}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String displayUserRegisterPage(Model model) throws Exception {
		model.addAttribute(new User());

		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@Valid User user, BindingResult bindingResult, Model model,
	                       @RequestParam("repassword") String repassword, String language, ModelAndView modelAndView) {
		if (!user.getPassword().equals(repassword)) {
			model.addAttribute("repasswordError", "Powtórz hasło");
		}
		if (bindingResult.hasErrors()) {
			return "/register";
		}
		user.setAccountCreationDate(new Date());
		user.setPassword(Crypto.digest(user.getPassword()));
		user.setActive(true);
		user.setRole(loginUtils.USER);

		String accountName = AccountNamer.generateAccountName(user);
		user.setAccountName(accountName);
//		userService.save(user);

        UserExtended userExtended = new UserExtended(user);
        userExtended.setObservedList(new ObservedList());
        userService.save(userExtended);

		return "redirect:/";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String displayUserLoginPage() {
		return "login";
	}

	@RequestMapping(value = "/app/credits", method = RequestMethod.GET)
	public ModelAndView credits() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("credits");

		return modelAndView;
	}

	@RequestMapping(value = "/app/locale/{language}")
	public ModelAndView changeLanguage(@PathVariable String language, HttpServletRequest request,
		HttpServletResponse response) {

		ModelAndView modelAndView = new ModelAndView();
		localeResolver.setLocale(request, response, new Locale(language));

		Cookie cookie = new Cookie("language", language);
		cookie.setMaxAge(2592000);
		cookie.setPath("/");

		response.addCookie(cookie);

		modelAndView.setViewName("redirect:/");

		return modelAndView;
	}
	@RequestMapping(value = "/app/tour")
	public ModelAndView tour() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("tour");

		return modelAndView;
	}
}
