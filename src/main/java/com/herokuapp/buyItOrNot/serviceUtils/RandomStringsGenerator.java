package com.herokuapp.buyItOrNot.serviceUtils;

import java.util.Date;
import java.util.Random;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 26.12.13 00:57
 */
public class RandomStringsGenerator {
	private static final String CHARS = "abcdefghijklmnoqprstuwvyz0123456789";

	public static String createRandomString(int lengthOfRandomizedPart) {
		StringBuilder builder = new StringBuilder();

		int range = CHARS.length();
		Random random = new Random(new Date().getTime());

		for (int i = 0; i < lengthOfRandomizedPart; i++) {
			char ch = CHARS.charAt(Math.abs(random.nextInt()) % range);
			builder.append(ch);
		}
		return builder.toString();
	}
}
