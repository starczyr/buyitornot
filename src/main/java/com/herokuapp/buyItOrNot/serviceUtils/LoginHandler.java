package com.herokuapp.buyItOrNot.serviceUtils;

import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.user.UserService;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * User: Radek Starczynowski
 * Date: 24.12.13
 * Time: 00:02
 */
public class LoginHandler implements AuthenticationSuccessHandler {
	private SessionLocaleResolver resolver;
	private UserService userService;

	public LoginHandler(SessionLocaleResolver resolver, UserService userService) {
		this.resolver = resolver;
		this.userService = userService;
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	                                    Authentication authentication) throws IOException, ServletException {
		String language = "en";

		for(Cookie cookie : request.getCookies()) {
			if(cookie.getName().equals("language")) {
				language = cookie.getValue();
			}
		}
		resolver.setLocale(request, response, new Locale(language));


		User user = userService.getUserByMail(authentication.getName());
		user.setLastLoginDate(new Date());
		userService.update(user);

        // TODO: set user.accountName istead of user object
		request.getSession().setAttribute("loggedInUser", user);
		//

		response.sendRedirect("/dashboard");

	}
}
