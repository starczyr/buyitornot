package com.herokuapp.buyItOrNot.serviceUtils;

import com.herokuapp.buyItOrNot.photo.Photo;
import com.herokuapp.buyItOrNot.photo.PhotoService;
import com.herokuapp.buyItOrNot.user.UserService;
import com.herokuapp.buyItOrNot.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Radek Starczynowski
 * <radek.starczynowski@gmail.com>
 * 30.01.14, 00:27
 */
public class Finder {
    public static void search(String string, UserService userService, PhotoService photoService) {
        if(string.startsWith("@")) {
            searchInUsers(string.replaceFirst("^@", ""), userService);
        } else if(string.startsWith("$")) {
            searchInPhotos(string.replaceFirst("^$", ""), photoService);
        } else {
            overallSearch(string, userService, photoService);
        }
    }

    private static void overallSearch(String string, UserService userService, PhotoService photoService) {
    }

    private static List<Photo> searchInPhotos(String string, PhotoService photoService) {
        return photoService.getPhotosLike(string);
    }

    private static List<User> searchInUsers(String string, UserService userService) {
       return userService.getUsersLike(string);
    }
}
