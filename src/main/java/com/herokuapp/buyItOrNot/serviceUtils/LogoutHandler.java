package com.herokuapp.buyItOrNot.serviceUtils;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * User: Radek Starczynowski
 * Date: 24.12.13
 * Time: 12:33
 */
public class LogoutHandler implements LogoutSuccessHandler {
	private SessionLocaleResolver resolver;

	public LogoutHandler(SessionLocaleResolver resolver) {
		this.resolver = resolver;
	}

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
	                            Authentication authentication) throws IOException, ServletException {
		String language = "en";

		for(Cookie cookie : request.getCookies()) {
			if(cookie.getName().equals("language")) {
				language = cookie.getValue();
			}
		}
		resolver.setLocale(request, response, new Locale(language));

		response.sendRedirect("/");
	}
}
