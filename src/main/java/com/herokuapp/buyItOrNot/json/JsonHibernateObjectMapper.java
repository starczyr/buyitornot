package com.herokuapp.buyItOrNot.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

/**
 * Radek Starczynowski
 * <radek.starczynowski@gmail.com>
 * 31.01.14, 14:38
 */
public class JsonHibernateObjectMapper extends ObjectMapper {
    public JsonHibernateObjectMapper() {
        registerModule(new Hibernate4Module());

    }
}
