package com.herokuapp.buyItOrNot.vote;

import com.herokuapp.buyItOrNot.user.model.User;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * User: Radek Starczynowski
 * Date: 13.12.13
 * Time: 23:36
 *
 */
@MappedSuperclass
public class Vote {
	public static enum VoteFor {
		YES,
		NO
	}
	@Id
	@Column(name = "id_vote", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_user")
	private User author;

	@Column(name = "voted_on", nullable = false)
	private Date votedOn;

	@Column(name = "vote_for", nullable = false)
	private VoteFor voteFor;

	public Vote() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Date getVotedOn() {
		return votedOn;
	}

	public void setVotedOn(Date votedOn) {
		this.votedOn = votedOn;
	}

	public VoteFor getVoteFor() {
		return voteFor;
	}

	public void setVoteFor(VoteFor voteFor) {
		this.voteFor = voteFor;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;

		Vote vote = (Vote) o;

		if(author != null ? !author.equals(vote.author) : vote.author != null) return false;
		if(votedOn != null ? !votedOn.equals(vote.votedOn) : vote.votedOn != null) return false;
		if(id != null ? !id.equals(vote.id) : vote.id != null) return false;
		if(voteFor != vote.voteFor) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (author != null ? author.hashCode() : 0);
		result = 31 * result + (votedOn != null ? votedOn.hashCode() : 0);
		result = 31 * result + (voteFor != null ? voteFor.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Vote{");
		sb.append("id=").append(id);
		sb.append(", author=").append(author);
		sb.append(", createdOn=").append(votedOn);
		sb.append(", voteFor=").append(voteFor);
		sb.append('}');
		return sb.toString();
	}
}
