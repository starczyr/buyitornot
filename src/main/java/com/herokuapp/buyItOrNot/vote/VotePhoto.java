package com.herokuapp.buyItOrNot.vote;

import com.herokuapp.buyItOrNot.photo.Photo;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * User: Radek Starczynowski
 * Date: 16.12.13
 * Time: 23:00
 */
@Entity
@Table(name = "public.vote_photo")
public class VotePhoto extends Vote {
	@ManyToOne
	@JoinColumn(name = "id_photo", insertable = false, updatable = false)
	private Photo voted;

	public Photo getVoted() {
		return voted;
	}

	public void setVoted(Photo voted) {
		this.voted = voted;
	}
}
