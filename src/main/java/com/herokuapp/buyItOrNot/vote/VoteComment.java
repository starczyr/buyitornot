package com.herokuapp.buyItOrNot.vote;

import com.herokuapp.buyItOrNot.comment.Comment;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * User: Radek Starczynowski <rstarczynowski@decerto.pl>
 * Date: 16.12.13
 * Time: 23:18
 */
@Entity
@Table(name = "public.vote_comment")
public class VoteComment extends Vote {
	@ManyToOne
	@JoinColumn(name = "id_comment", insertable = false, nullable = false, updatable = false)
	private Comment voted;

	public Comment getVoted() {
		return voted;
	}

	public void setVoted(Comment voted) {
		this.voted = voted;
	}
}
