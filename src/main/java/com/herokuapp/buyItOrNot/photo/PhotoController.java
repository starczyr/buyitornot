package com.herokuapp.buyItOrNot.photo;

import com.herokuapp.buyItOrNot.comment.Comment;
import com.herokuapp.buyItOrNot.serviceUtils.LoginUtils;
import com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading.PhotoCache;
import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.user.UserFetcher;
import com.herokuapp.buyItOrNot.user.UserService;
import com.herokuapp.buyItOrNot.user.model.UserExtended;
import com.herokuapp.buyItOrNot.vote.Vote;
import com.herokuapp.buyItOrNot.vote.VotePhoto;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.management.MalformedObjectNameException;

/**
 * User: Radek Starczynowski
 * Date: 17.12.13
 * Time: 01:49
 */
@Controller
@RequestMapping("/photo")
public class PhotoController {
	@Autowired
	private PhotoService photoService;
	@Autowired
	private UserService userService;
	@Autowired
	private LoginUtils loginUtils;

	public PhotoController() {
	}



    @RequestMapping(value = "/like/{pattern}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<Photo> getPhotosLike(@PathVariable String pattern) {
        return photoService.getPhotosLike(pattern);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView getPhoto(@PathVariable Long id) {
        Photo photo = photoService.findById(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("photo", photo);
        modelAndView.setViewName("photo");

        return modelAndView;
    }

	@RequestMapping(value = "/{id}/vote", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void voteYes(@PathVariable Long id, String vote) {
		Photo photo = photoService.findById(id);
		User user = userService.getUserByMail(loginUtils.getLoggedInUserMail());

		VotePhoto votePhoto = new VotePhoto();
		votePhoto.setVoted(photo);
		votePhoto.setAuthor(user);
		votePhoto.setVotedOn(new Date());
		votePhoto.setVoteFor(Vote.VoteFor.valueOf(vote));

		photo.getVotes().add(votePhoto);
		photoService.update(photo);
	}


	@RequestMapping(value = "/{id}/votes/count", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody VotesCounter getVotesCounter(@PathVariable Long id) {
		Photo photo = photoService.findById(id);

		return photoService.countVotes(photo);
	}

    /**
     * Save all uris (if exist) send in uris map
     *
     * @param caches
     * @return
     * @throws Exception
     */
	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	String saveCaches(@RequestBody List<Map<String, String>> caches) throws Exception {
		UserExtended userExtended = userService.getUserExtendedWithItemsByMail(loginUtils.getLoggedInUserMail(),
                new UserFetcher().fetch(UserFetcher.Item.PHOTOS));
        if(caches.size() == 0) {
            return "error";
        }
		for(Map cache : caches) {
            try {
                String uri = (String) cache.get("uri");
                if(uri == null) {
                    // TODO: create own exception
                    throw new MalformedObjectNameException();
                }
                String name = (String) cache.get("name");
                if(name == null) {
                    // TODO: create own exception
                    throw new MalformedObjectNameException();
                }
                String description = (String) cache.get("description");
                if(description == null) {
                    // TODO: create own exception
                    throw new MalformedObjectNameException();
                }
                //
			    PhotoCache photoCache = photoService.getCache(uri);
			    String url = photoService.saveFile(photoCache, userExtended.getUser());
			    Photo photo = new Photo(name, Photo.Status.BUY_OR_NOT, url);
                photo.setDescription(description);
			    photo.setOwner(userExtended.getUser());
			    photo.setAddedOn(new Date());

                userExtended.getPhotos().add(photo);
			    userService.update(userExtended);
            } catch(Exception e) {
                return "Error";
            }
		}
		return "ok";
}

	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/cache/add", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	String cacheFile(@RequestParam("file") MultipartFile file) throws Exception {
		if (!file.isEmpty()) {
			try {
				return photoService.cachePhoto(file);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

    @PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
    @RequestMapping(value = "/cache/delete", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    String deleteCache(String cache) throws Exception {
        if (cache != null) {
            photoService.deleteCache(cache);
            return "OK";
        }
        return "Error";
    }

    @PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView upload() throws Exception {
        ModelAndView modelAndView = new ModelAndView("upload");
        modelAndView.addObject("tempDirName", photoService.getTempDirName());

        return modelAndView;
    }

	@PreAuthorize("@loginUtils.isUser() or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{id}/comment/add", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public ModelAndView addComment(@PathVariable Long id, @RequestParam("comment") String comment) {
		ModelAndView modelAndView = new ModelAndView();
		User user = userService.getUserByMail(loginUtils.getLoggedInUserMail());

		// TODO: validate comment

        Photo photo = photoService.findById(id);

		Comment photoComment = new Comment();
		photoComment.setAddedOn(new Date());
		photoComment.setContent(comment);
		photoComment.setAuthor(user);
		photoComment.setPhoto(photo);

//        // TODO: do like that only if required
//        userExtended.getComments().add(photoComment);
//        userService.update(userExtended);
//        //

		photo.getComments().add(photoComment);
		photoService.update(photo);

		modelAndView.setViewName("redirect:/dashboard");

		return modelAndView;
	}

	@PreAuthorize("(@loginUtils.isUser() and " +
			"@photoService.getOwnerMail(#id) == @loginUtils.getLoggedInUserMail())" +
			" or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{id}/bought", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public ModelAndView markAsBought(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Photo photo = photoService.findById(id);

		photo.setStatus(Photo.Status.BOUGHT);
		photoService.update(photo);

		modelAndView.setViewName("redirect:/dashboard");

		return modelAndView;
	}

	@PreAuthorize("(@loginUtils.isUser() and " +
			"@photoService.getOwnerMail(#id) == @loginUtils.getLoggedInUserMail())" +
			" or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{id}/didntBuy", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public ModelAndView markAsDidntBuy(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Photo photo = photoService.findById(id);

		photo.setStatus(Photo.Status.DIDNT_BUY);
		photoService.update(photo);

		modelAndView.setViewName("redirect:/dashboard");

		return modelAndView;
	}

	@PreAuthorize("(@loginUtils.isUser() and " +
			"@photoService.getOwnerMail(#id) == @loginUtils.getLoggedInUserMail())" +
			" or @loginUtils.isAdmin()")
	@RequestMapping(value = "/{id}/buyOrNot", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public ModelAndView markAsBuyOrNot(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Photo photo = photoService.findById(id);

		photo.setStatus(Photo.Status.BUY_OR_NOT);
		photoService.update(photo);

		modelAndView.setViewName("redirect:/dashboard");

		return modelAndView;
	}

    // TODO: delete Photo method
}

