
package com.herokuapp.buyItOrNot.photo;

import com.herokuapp.buyItOrNot.comment.Comment;
import com.herokuapp.buyItOrNot.user.model.User;
import com.herokuapp.buyItOrNot.vote.VotePhoto;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * User: Radek Starczynowski
 * Date: 13.12.13
 * Time: 15:30
 */
@Entity
@Table(name = "public.photo")
public class Photo {
	public static enum Status {
		BUY_OR_NOT,
		BOUGHT,
		DIDNT_BUY
	}
	@Id
	@Column(name = "id_photo", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "url", nullable = false, unique = true)
	private String url;

	@Column(name = "added_on", nullable = false)
	private Date addedOn;

	@ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "id_user", nullable = false)
	private User owner;

	@Column(name = "status", nullable = false)
	private Status status;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "photo", cascade = CascadeType.ALL)
	@OrderBy(value = "added_on ASC")
	private List<Comment> comments;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "voted", cascade = CascadeType.ALL)
	private Set<VotePhoto> votes;

	public Photo() {
	}

	public Photo(String name, Status status, String url) {
		this.name = name;
		this.status = status;
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public User getOwner() {
		return owner;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Set<VotePhoto> getVotes() {
		return votes;
	}

	public void setVotes(Set<VotePhoto> votes) {
		this.votes = votes;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;

		Photo photo = (Photo) o;

		if(description != null ? !description.equals(photo.description) : photo.description != null) return false;
		if(!id.equals(photo.id)) return false;
		if(name != null ? !name.equals(photo.name) : photo.name != null) return false;
		if(!owner.equals(photo.owner)) return false;
		if(!url.equals(photo.url)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + url.hashCode();
		result = 31 * result + owner.hashCode();
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Photo{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", url='").append(url).append('\'');
		sb.append(", owner=").append(owner);
		sb.append('}');
		return sb.toString();
	}
}
