package com.herokuapp.buyItOrNot.photo;

import com.herokuapp.buyItOrNot.vote.Vote;
import com.herokuapp.buyItOrNot.vote.VotePhoto;

import java.util.Set;

/**
 * Radek Starczynowski
 * radek.starczynowski@gmail.com
 * 06.01.14 19:00
 */
public class VotesCounter {
	private int votesForYes;
	private int votesForNo;

	public VotesCounter(Set<VotePhoto> votes) {
		for(Vote vote : votes) {
			if(vote.getVoteFor() == Vote.VoteFor.YES) {
				votesForYes++;
			} else {
				votesForNo++;
			}
		}
	}

	public int getVotesForYes() {
		return votesForYes;
	}

	public int getVotesForNo() {
		return votesForNo;
	}
}
