package com.herokuapp.buyItOrNot.photo;

import com.herokuapp.buyItOrNot.dao.DAO;
import com.herokuapp.buyItOrNot.user.model.User;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * User: Radek Starczynowski
 * Date: 18.12.13
 * Time: 17:22
 */
@Repository
public class PhotoDAO implements DAO<Photo> {
	private SessionFactory sessionFactory;

	public PhotoDAO() {
	}

	@Autowired
	public PhotoDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Photo getById(Long id) {
		return (Photo) getSession().get(Photo.class, id);
	}

	@Override
	public void delete(Photo photo) {
		getSession().delete(photo);
	}

	@Override
	public void update(Photo photo) {
		getSession().update(photo);
	}

	@Override
	public void save(Photo photo) {
		getSession().save(photo);
	}

	@SuppressWarnings("unchecked")
	public List<Photo> getPhotos(User user, Photo.Status status) {
		return getSession().createQuery("from com.herokuapp.buyItOrNot.photo.Photo as photo " +
				"where photo.status = :status and photo.owner.id = :id_user")
				.setParameter("status", status)
				.setParameter("id_user", user.getId()).list();
	}

    @SuppressWarnings("unchecked")
    public List<Photo> getPhotosLike(String like) {
        StringBuilder builder = new StringBuilder(like);
        builder.insert(0, '%').insert(builder.length(), '%');
        return (List<Photo>) getSession().createQuery("from com.herokuapp.buyItOrNot.photo.Photo as photo " +
                "full join photo.owner " +
                "where photo.name like :like").setParameter("like", builder.toString()).list();
    }
}
