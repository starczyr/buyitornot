package com.herokuapp.buyItOrNot.photo;

import java.util.Comparator;

/**
 * Radek Starczynowski
 * <radek.starczynowski@gmail.com>
 * 02.02.14, 22:58
 */
public class PhotoComparator implements Comparator<Photo> {
    @Override
        public int compare(Photo photo1, Photo photo2) {
            return photo2.getAddedOn().compareTo(photo1.getAddedOn());
    }
}
