package com.herokuapp.buyItOrNot.photo;

import com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading.FileSaver;
import com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading.PhotoCache;
import com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading.PhotoCacher;
import com.herokuapp.buyItOrNot.serviceUtils.photoCachingAndUploading.PhotosDirHolder;
import com.herokuapp.buyItOrNot.user.model.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.herokuapp.buyItOrNot.user.model.UserExtended;
import org.apache.commons.fileupload.FileUploadException;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * User: Radek Starczynowski
 * Date: 18.12.13
 * Time: 17:28
 */
@Service
@Transactional(readOnly = true)
public class PhotoService {
	@Autowired
	private PhotosDirHolder photosDirHolder;
	private PhotoDAO photoDAO;

	public PhotoService() {
	}

	@Autowired
	public PhotoService(PhotoDAO photoDAO) {
		this.photoDAO = photoDAO;
	}

	public Photo findById(Long id) {
		return photoDAO.getById(id);
	}

	@Transactional(readOnly = false)
	public void update(Photo photo) {
		photoDAO.update(photo);
	}

	@Transactional(readOnly = false)
	public void delete(Photo photo) {
		photoDAO.delete(photo);
	}

	@Transactional(readOnly = false)
	public void save(Photo photo) {
		photoDAO.save(photo);
	}

	@Transactional(readOnly = false)
	public void changePhotoStatus(Long photoId, Photo.Status toStatus) {
		Photo photo = findById(photoId);
		photo.setStatus(toStatus);
		update(photo);
	}

	public List<Photo> getPhotos(User user, Photo.Status status) {
		return photoDAO.getPhotos(user, status);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public List<Photo> getStatusPhotos(UserExtended userExtended, Photo.Status status) {
		List<Photo> allPhotos;
		try {
			allPhotos = userExtended.getPhotos();
		} catch(LazyInitializationException e) {
			throw new RuntimeException(e.getMessage() + " (probably You forgot to fetch data)");
		}
		List<Photo> result = new ArrayList<Photo>();
		for(Photo photo : allPhotos) {
			if(photo.getStatus() == status) {
				result.add(photo);
			}
		}
		return result;
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public String getTempDirName() {
		return photosDirHolder.getTempDir().getName();
	}
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public String cachePhoto(MultipartFile file) throws IOException {
		return PhotoCacher.cachePhoto(file, photosDirHolder.getTempDir());
	}

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void deleteCache(String cache) {
        PhotoCacher.deleteCache(cache);
    }

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public String saveFile(PhotoCache photoCache, User user) throws FileUploadException, IOException {
		File saved = FileSaver.saveFile(photoCache, photosDirHolder.getUserFolder(user));
		return photosDirHolder.convertSavedPhotoRealtoContextPath(saved.getAbsolutePath());
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public String saveFile(MultipartFile file, User user) throws FileUploadException, IOException {
		File saved = FileSaver.saveFile(file, photosDirHolder.getUserFolder(user));
		return photosDirHolder.convertSavedPhotoRealtoContextPath(saved.getAbsolutePath());
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public User getOwner(Long id) {
		return photoDAO.getById(id).getOwner();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getOwnerMail(Long id) {
		return photoDAO.getById(id).getOwner().getMail();
	}

	public PhotoCache getCache(String cacheName) {
		return PhotoCacher.getCache(cacheName);
	}

	public VotesCounter countVotes(Photo photo) {
		return new VotesCounter(photo.getVotes());
	}

    @SuppressWarnings("unchecked")
    public List<Photo> getPhotosLike(String like) {
        return photoDAO.getPhotosLike(like);
    }
}
