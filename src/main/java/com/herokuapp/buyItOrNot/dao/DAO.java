package com.herokuapp.buyItOrNot.dao;

/**
 * User: Radek Starczynowski
 * Date: 25.11.13
 * Time: 22:48
 */
public interface DAO<T> {
	public T getById(Long id);

	public void delete(T t);

	public void update(T t);

	public void save(T t);
}
