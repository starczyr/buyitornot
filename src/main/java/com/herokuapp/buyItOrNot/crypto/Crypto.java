package com.herokuapp.buyItOrNot.crypto;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * User: Radek Starczynowski
 * Date: 01.12.13
 * Time: 21:59
 */
public class Crypto {
	public static String digest(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
}
