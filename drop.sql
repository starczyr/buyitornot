drop table comment cascade;
drop table observed_list cascade;
drop table observed_users cascade;
drop table photo cascade;
drop table "user" cascade;
drop table user_extended cascade;
drop table vote_comment cascade;
drop table vote_photo cascade;